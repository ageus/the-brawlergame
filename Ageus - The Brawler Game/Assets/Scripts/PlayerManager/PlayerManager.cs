﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerManager : MonoBehaviour
{
    [Header("Required for UI")]
    public SpriteRenderer[] UIControlPlayer = new SpriteRenderer[4];
    public Text UICountdownText;
    public Image UIImage;
    public Camera cam;

    [Header("Required for Player")]
    public Transform[] playerSpawn = new Transform[4];

    [Header("Required for Creator")]
    public bool[] playerCreatorActive = new bool[4];

    [Header("Other stuff")]
    public int activePlayers = 0;
    public int readyPlayers = 0;
    public int oldActivePlayers = 0;
    public int oldReadyPlayers = 0;

    //Color32 playerInactive = new Color32(0, 0, 0, 225);
    //Color32 playerActive = new Color32(255, 50, 50, 225);
    Color32 imageInactive = new Color32(0, 0, 0, 0);
    Color32 imageActive = new Color32(0, 0, 0, 100);

    private bool[] playerActive = new bool[4];

    private bool[] playerStartGame = new bool[4];

    private GameObject[] playerCreator = new GameObject[4];

    private Transform[] characterSpawn = new Transform[4];

    //Global Cool Downs
    private bool[] playerGlobalCooldown = new bool[4];

    Sprite[] temp_Sprites;

    //To Start the game
#if UNITY_EDITOR
    float gameStartCountDown = 0f;
#else
    float gameStartCountDown = 5f;
#endif

    bool countDownRunning = false;

    bool gameStarted = false;

    // Use this for initialization
    void Start()
    {
        for (int i = 0; i <= 3; i++)
        {
            UIControlPlayer[i].gameObject.SetActive(false);
            playerStartGame[i] = false;
            playerActive[i] = false;
            characterSpawn[i] = UIControlPlayer[i].transform.Find("CharSelBackground/UISpawn");
        }

        UIImage.color = imageActive;
        temp_Sprites = Resources.LoadAll<Sprite>("UI/Player_Schrift");
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameStarted)
        {
            #region Check how many Joysticks are connected
            for (int i = 0; i <= 3; i++)
            {
                UIControlPlayer[i].gameObject.SetActive(false);
            }
            for (int i = 0; i < Input.GetJoystickNames().Length; i++)
            {
                UIControlPlayer[i].gameObject.SetActive(true);
            }
            #endregion

            #region Activate/Deactivate Controller
            //Check if Controller 1 is active and Activate/Deactivate it
            for (int i = 0; i <= 3; i++)
            {
                if (!playerCreator[i])
                {
                    if (Input.GetButton("P" + i + " Button A") && !playerActive[i] && !playerGlobalCooldown[i])
                    {
                        Activate(i);
                    }
                    else if (Input.GetButtonUp("P" + i + " Button A") && playerGlobalCooldown[i])
                    {
                        playerGlobalCooldown[i] = false;
                    }

                    if (Input.GetButton("P" + i + " Button B") && playerActive[i] && !playerGlobalCooldown[i])
                    {
                        Deactivate(i);
                    }
                    else if (Input.GetButtonUp("P" + i + " Button B") && playerGlobalCooldown[i])
                    {
                        playerGlobalCooldown[i] = false;
                    }
                }
            }
            #endregion

            #region Start the Game
            // Start the cooldown when every active player is ready
            if (activePlayers == readyPlayers && activePlayers > 0 && readyPlayers > 0)
            {
                if (!countDownRunning)
                {
                    //Debug.Log("STARTING COUNTDOWN!");
                    StartCoroutine("GameStartCountDown");
                    oldActivePlayers = activePlayers;
                    oldReadyPlayers = readyPlayers;
                }
            }
            else
            {
                if (oldActivePlayers != activePlayers)
                {
                    //Debug.Log("ACTIVE PLAYERS CHANGED!");
                    oldActivePlayers = activePlayers;
                    UICountdownText.text = "";
                    StopCoroutine("GameStartCountDown");
                    countDownRunning = false;
                }
                if (oldReadyPlayers != readyPlayers)
                {
                    //Debug.Log("READY PLAYERS CHANGED!");
                    oldReadyPlayers = readyPlayers;
                    UICountdownText.text = "";
                    StopCoroutine("GameStartCountDown");
                    countDownRunning = false;
                }
            }
            #endregion
        }
    }

    void Activate(int player)
    {
        Object temp = Instantiate(Resources.Load("Player/Player"));

        //UIControlPlayer1.color = playerActive;
        Transform temp_UI = UIControlPlayer[player].transform.Find("CharSelBackground");

        switch (player)
        {
            case 0:
                UIControlPlayer[player].GetComponent<SpriteRenderer>().sprite = temp_Sprites[0];
                break;
            case 1:
                UIControlPlayer[player].GetComponent<SpriteRenderer>().sprite = temp_Sprites[2];
                break;
            case 2:
                UIControlPlayer[player].GetComponent<SpriteRenderer>().sprite = temp_Sprites[4];
                break;
            case 3:
                UIControlPlayer[player].GetComponent<SpriteRenderer>().sprite = temp_Sprites[6];
                break;
            default:
                break;
        }

        temp_UI.gameObject.SetActive(true);

        temp.name = "Player" + player;
        (temp as GameObject).transform.position = characterSpawn[player].position;
        (temp as GameObject).transform.rotation = characterSpawn[player].rotation;
        playerCreator[player] = GameObject.Instantiate(Resources.Load("Player/P_Creator")) as GameObject;
        playerCreator[player].name = "P" + player + "_Creator";
        playerCreator[player].transform.SetParent(this.transform);
        playerCreator[player].GetComponent<PlayerCreator>().player = player;

        activePlayers++;
        playerActive[player] = true;
        playerGlobalCooldown[player] = true;
        playerCreatorActive[player] = true;
    }

    public void Deactivate(int player)
    {
        //UIControlPlayer1.color = playerInactive;
        Transform temp_UI = UIControlPlayer[player].transform.Find("CharSelBackground");
        Sprite[] temp_Sprites = Resources.LoadAll<Sprite>("UI/Player_Schrift");

        switch (player)
        {
            case 0:
                UIControlPlayer[player].GetComponent<SpriteRenderer>().sprite = temp_Sprites[1];
                break;
            case 1:
                UIControlPlayer[player].GetComponent<SpriteRenderer>().sprite = temp_Sprites[3];
                break;
            case 2:
                UIControlPlayer[player].GetComponent<SpriteRenderer>().sprite = temp_Sprites[5];
                break;
            case 3:
                UIControlPlayer[player].GetComponent<SpriteRenderer>().sprite = temp_Sprites[7];
                break;
            default:
                break;
        }

        temp_UI.gameObject.SetActive(false);

        Destroy(playerCreator[player]);
        Destroy(GameObject.Find("Player" + (player)));

        activePlayers--;
        playerActive[player] = false;
        playerGlobalCooldown[player] = true;
        playerCreatorActive[player] = false;
    }

    public void PlayerReady(int player)
    {
        if (!playerStartGame[player])
        {
            playerStartGame[player] = true;
            readyPlayers++;
        }
    }

    public void PlayerNotReady(int player)
    {
        playerStartGame[player] = false;
        readyPlayers--;
    }

    IEnumerator GameStartCountDown()
    {
        countDownRunning = true;
        for (float i = gameStartCountDown; i > 0; i--)
        {
            Debug.Log(i + " seconds to start");
            UICountdownText.text = i.ToString();
            yield return new WaitForSeconds(1);
        }
        UICountdownText.text = "";
        Debug.Log("START THE GAME!");
        StartingGame();
    }

    void StartingGame()
    {
        gameStarted = true;
        GameManager gm = GetComponent<GameManager>();

        for (int i = 0; i <= 3; i++)
        {
            if (playerStartGame[i])
            {
                Transform temp_UI = UIControlPlayer[i].transform.Find("CharSelBackground");
                temp_UI.gameObject.SetActive(false);

                GameObject GO = GameObject.Find("Player" + i);
                GO.transform.position = playerSpawn[i].position;
                GO.GetComponent<CharController>().enabled = true;
                GO.GetComponent<CharController>().StartControls();
                Destroy(playerCreator[i]);
                playerCreator[i].GetComponent<PlayerCreator>().selectedButtonGameObject.SetActive(false);

                UIControlPlayer[i].gameObject.SetActive(false);

                gm.Players.Add(i, true);
            }
        }

        UIImage.color = imageInactive;
        UIImage.enabled = false;
    }
}
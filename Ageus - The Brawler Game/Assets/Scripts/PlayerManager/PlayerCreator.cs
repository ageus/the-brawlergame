﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class Spell
{
    public Spell(GameObject spellObj)
    {
        spellObject = spellObj;
        prefabName = spellObj.name;
    }

    public GameObject spellObject { get; set; }
    public string prefabName { get; set; }
}

public class PlayerCreator : MonoBehaviour
{
    [Header("Player Settings")]
    public int player = 0;
    public int selectedCharacter = 1;
    public int selectedButton;
    public bool selectingCharacter = false;
    public bool selectingSpells = false;
    public bool selectingSingleSpell = false;
    public bool selectingPassive = false;
    public bool playerReady = false;

    //GCD
    WaitForSeconds thumbstick_GCD = new WaitForSeconds(0.2f);
    WaitForSeconds buttonA_GCD = new WaitForSeconds(0.5f);
    WaitForSeconds buttonB_GCD = new WaitForSeconds(0.5f);
    bool thumbstick_GCDOn = false;
    bool buttonA_GCDOn = false;
    bool buttonB_GCDOn = false;
    bool buttonAPressed = false;
    bool buttonBPressed = false;

    //Just to controll some stuff
    public GameObject thisPlayer;
    public GameObject thisPlayerRotationObject;
    PlayerManager thisPlayerManager;
    GameObject Character;

    //Gotta Control the UI, don't I?
    public GameObject selectedButtonGameObject;
    GameObject selectableSpellsGameObject;
    GameObject selectablePassivesGameObject;
    GameObject selectionBorderSpellsGameObject;
    GameObject selectionBorderPassivesGameObject;
    //x range: -75, -25, +25, +75, y range: -250, -290, -330, -370, Choosing Spells
    string choosingSpell = "";
    string oldChoosingSpell = "";
    //int choosingPassive = 1;

    public Dictionary<string, Spell> spells = new Dictionary<string, Spell>();

    public List<GameObject> selectableButtons;
    public List<GameObject> sSpellTints;

    //required for health
    PlayerHealthController PHC;

    //Not needed Currently
    bool foundPlayer = false;

    int oldSelSpell = 0;

    // Use this for initialization
    private void Start()
    {
        selectedCharacter = player + 1;

        //Finding this Player and Setting some stuff
        thisPlayer = GameObject.Find("Player" + player);
        thisPlayerRotationObject = thisPlayer.transform.Find("RotationObject").gameObject;
        thisPlayerManager = this.gameObject.GetComponentInParent<PlayerManager>();
        thisPlayer.GetComponent<CharController>().player = player;
        thisPlayer.GetComponent<CharController>().StopControls();
        thisPlayer.GetComponent<CharController>().character = selectedCharacter;
        selectingCharacter = true;

        //Creating the Std. Character
        Character = Instantiate(Resources.Load("Character/Character" + selectedCharacter), thisPlayer.transform.position, thisPlayer.transform.rotation) as GameObject;
        Character.name = Character.name.Remove(10);
        Character.transform.SetParent(thisPlayerRotationObject.transform);

        //Starting the Global Cooldowns once
        StartCoroutine(ButtonA_GCD());
        StartCoroutine(ButtonB_GCD());

        //Setting everything in the Player to the Layer UI and Setting the Position accordingly
        thisPlayer.transform.position = new Vector3(thisPlayer.transform.position.x, thisPlayer.transform.position.y, -0.5f);

        //Initializing UI and everything it needs
        selectedButtonGameObject = GameObject.Find("Canvas/UI_Controller" + player + "/SelectedButtons");
        selectableSpellsGameObject = GameObject.Find("Canvas/UI_Controller" + player + "/SelectableSpells");
        selectablePassivesGameObject = GameObject.Find("Canvas/UI_Controller" + player + "/SelectablePassives");
        selectionBorderSpellsGameObject = GameObject.Find("Canvas/UI_Controller" + player + "/SelectableSpells/SelectionBorder");
        selectionBorderPassivesGameObject = GameObject.Find("Canvas/UI_Controller" + player + "/SelectablePassives/SelectionBorder");
        selectedButtonGameObject.SetActive(false);
        selectableSpellsGameObject.SetActive(false);
        selectablePassivesGameObject.SetActive(false);

        bool objectsDone = false;
        int counter = 1;
        while (!objectsDone)
        {
            if (counter <= 6)
            {
                selectableButtons.Add(GameObject.Find("Canvas/UI_Controller" + player + "/SelectedButtons/SelectedButton" + counter));
                sSpellTints.Add(GameObject.Find("Canvas/UI_Controller" + player + "/SelectedButtons/SelectedButtonTint" + counter));
                counter++;
            }
            else if (counter > 6)
            {
                objectsDone = true;
            }
        }

        int spellMod = 0;
        switch (selectedCharacter)
        {
            case 1:
                spellMod = 0;
                break;
            case 2:
                spellMod = 4;
                break;
            case 3:
                spellMod = 8;
                break;
            case 4:
                spellMod = 12;
                break;
            default:
                break;
        }

        for (int j = 1; j <= 4; j++)
        {
            spells["Spell" + (spellMod + j)] = new Spell(GameObject.Instantiate(Resources.Load("Spells/Prefabs/Spell" + (spellMod + j))) as GameObject);
        }
        spells["Passive" + (selectedCharacter)] = new Spell(GameObject.Instantiate(Resources.Load("Spells/Prefabs/Passive" + (selectedCharacter))) as GameObject);

        int i = 0;
        foreach (KeyValuePair<string, Spell> entry in spells)
        {
            entry.Value.spellObject.transform.SetParent(thisPlayerRotationObject.transform, false);

            switch (i)
            {
                case 0:
                    entry.Value.spellObject.name = "Spell_LB";
                    break;
                case 1:
                    entry.Value.spellObject.name = "Spell_LT";
                    break;
                case 2:
                    entry.Value.spellObject.name = "Spell_RT";
                    break;
                case 3:
                    entry.Value.spellObject.name = "Spell_RB";
                    break;
                case 4:
                    entry.Value.spellObject.name = "Passive";
                    break;
                default:
                    break;
            }
            selectableButtons[i].GetComponent<Image>().sprite = Resources.Load<Sprite>("Spells/Sprites/Icon_" + entry.Key);
            i++;
        }

        PHC = Character.transform.root.GetComponent<PlayerHealthController>();
        PHC.healthObject = Character.transform.Find("Health").gameObject;
        PHC.AddLifePartsToList();
    }

    // Update is called once per frame
    private void Update()
    {
        #region Left Thumbstick Controls
        if (!thumbstick_GCDOn)
        {
            #region Selecting Character
            if (selectingCharacter)
            {
                if (Input.GetAxis("P" + player + "L Horizontal") > 0.7)
                {
                    if (selectedCharacter <= 4)
                    {
                        selectedCharacter++;
                    }
                    if (selectedCharacter > 4)
                    {
                        selectedCharacter = 1;
                    }
                    CharSelChanged();
                    StartCoroutine(Thumbstick_GCD());
                }
                if (Input.GetAxis("P" + player + "L Horizontal") < -0.7)
                {
                    if (selectedCharacter >= 1)
                    {
                        selectedCharacter--;
                    }
                    if (selectedCharacter < 1)
                    {
                        selectedCharacter = 4;
                    }
                    CharSelChanged();
                    StartCoroutine(Thumbstick_GCD());
                }
            }
            #endregion

            #region Selecting Spell
            if (selectingSpells)
            {
                if (Input.GetAxis("P" + player + "L Horizontal") < -0.7)
                {
                    //Proceed to select the previous spell!
                    if (selectedButton >= 1 && selectedButton <= sSpellTints.Count - 2)
                    {
                        selectedButton--;
                        oldSelSpell = selectedButton;
                        StartCoroutine(Thumbstick_GCD());
                        SelectionTintChange(selectedButton);
                    }
                    else if (selectedButton == 0)
                    {
                        selectedButton = sSpellTints.Count - 2;
                        oldSelSpell = selectedButton;
                        StartCoroutine(Thumbstick_GCD());
                        SelectionTintChange(selectedButton);
                    }
                }
                if (Input.GetAxis("P" + player + "L Horizontal") > 0.7)
                {
                    //Proceed to select the next spell!
                    if (selectedButton < sSpellTints.Count - 2)
                    {
                        selectedButton++;
                        oldSelSpell = selectedButton;
                        StartCoroutine(Thumbstick_GCD());
                        SelectionTintChange(selectedButton);
                    }
                    else if (selectedButton == sSpellTints.Count - 2)
                    {
                        selectedButton = 0;
                        oldSelSpell = selectedButton;
                        StartCoroutine(Thumbstick_GCD());
                        SelectionTintChange(selectedButton);
                    }
                }
                if (Input.GetAxis("P" + player + "L Vertical") < -0.7 || Input.GetAxis("P" + player + "L Vertical") > 0.7)
                {
                    if (selectedButton == 5)
                    {
                        selectedButton = oldSelSpell;
                        StartCoroutine(Thumbstick_GCD());
                        SelectionTintChange(selectedButton);
                    }
                    else if (selectedButton < 5)
                    {
                        oldSelSpell = selectedButton;
                        selectedButton = 5;
                        StartCoroutine(Thumbstick_GCD());
                        SelectionTintChange(selectedButton);
                    }
                }
            }
            #endregion

            #region Selecting Single Spell
            if (selectingSingleSpell)
            {
                //Differentiate between spells and passives
                if (selectedButton <= 3)
                {
                    if (Input.GetAxis("P" + player + "L Horizontal") < -0.7f)
                    {
                        //Left
                        int temp = int.Parse(choosingSpell.Remove(0, 5));

                        if ((temp > 1 && temp <= 4) || (temp > 5 && temp <= 8) || (temp > 9 && temp <= 12) || (temp > 13 && temp <= 16))
                        {
                            oldChoosingSpell = choosingSpell;
                            choosingSpell = "Spell" + (temp - 1);

                            RedSquarCtrl();
                        }
                        else if (temp == 1 || temp == 5 || temp == 9 || temp == 13)
                        {
                            oldChoosingSpell = choosingSpell;
                            choosingSpell = "Spell" + (temp + 3);

                            RedSquarCtrl();
                        }

                        StartCoroutine(Thumbstick_GCD());
                    }
                    if (Input.GetAxis("P" + player + "L Horizontal") > 0.7f)
                    {
                        //Right
                        int temp = int.Parse(choosingSpell.Remove(0, 5));

                        if ((temp >= 1 && temp < 4) || (temp >= 5 && temp < 8) || (temp >= 9 && temp < 12) || (temp >= 13 && temp < 16))
                        {
                            oldChoosingSpell = choosingSpell;
                            choosingSpell = "Spell" + (temp + 1);

                            RedSquarCtrl();
                        }
                        else if (temp == 4 || temp == 8 || temp == 12 || temp == 16)
                        {
                            oldChoosingSpell = choosingSpell;
                            choosingSpell = "Spell" + (temp - 3);

                            RedSquarCtrl();
                        }
                        StartCoroutine(Thumbstick_GCD());
                    }
                }
                if (Input.GetAxis("P" + player + "L Vertical") < -0.7)
                {
                    //Up

                    //Active Spells
                    if (selectedButton <= 3)
                    {
                        int temp = int.Parse(choosingSpell.Remove(0, 5));

                        if ((temp >= 5 && temp <= 13) || (temp >= 6 && temp <= 14) || (temp >= 7 && temp <= 15) || (temp >= 8 && temp <= 16))
                        {
                            oldChoosingSpell = choosingSpell;
                            choosingSpell = "Spell" + (temp - 4);

                            RedSquarCtrl();
                        }
                        else if (temp == 1 || temp == 2 || temp == 3 || temp == 4)
                        {
                            oldChoosingSpell = choosingSpell;
                            choosingSpell = "Spell" + (temp + 12);

                            RedSquarCtrl();
                        }
                    }
                    //Passive Spells
                    else if (selectedButton == 4)
                    {
                        int temp = int.Parse(choosingSpell.Remove(0, 7));

                        if (temp > 1)
                        {
                            oldChoosingSpell = choosingSpell;
                            choosingSpell = "Passive" + (temp - 1);
                            RedSquarCtrl();
                        }
                        else if (temp == 1)
                        {
                            oldChoosingSpell = choosingSpell;
                            choosingSpell = "Passive4";
                            RedSquarCtrl();
                        }
                    }
                    StartCoroutine(Thumbstick_GCD());
                }
                if (Input.GetAxis("P" + player + "L Vertical") > 0.7)
                {
                    //Down

                    //Active Spells
                    if (selectedButton <= 3)
                    {
                        int temp = int.Parse(choosingSpell.Remove(0, 5));

                        if ((temp >= 1 && temp <= 9) || (temp >= 2 && temp <= 10) || (temp >= 3 && temp <= 11) || (temp >= 4 && temp <= 12))
                        {
                            oldChoosingSpell = choosingSpell;
                            choosingSpell = "Spell" + (temp + 4);

                            RedSquarCtrl();
                        }
                        else if (temp == 13 || temp == 14 || temp == 15 || temp == 16)
                        {
                            oldChoosingSpell = choosingSpell;
                            choosingSpell = "Spell" + (temp - 12);

                            RedSquarCtrl();
                        }
                    }
                    //Passive Spells
                    else if (selectedButton == 4)
                    {
                        int temp = int.Parse(choosingSpell.Remove(0, 7));

                        if (temp < 4)
                        {
                            oldChoosingSpell = choosingSpell;
                            choosingSpell = "Passive" + (temp + 1);
                            RedSquarCtrl();
                        }
                        else if (temp == 4)
                        {
                            oldChoosingSpell = choosingSpell;
                            choosingSpell = "Passive1";
                            RedSquarCtrl();
                        }
                    }
                    StartCoroutine(Thumbstick_GCD());
                }
            }
            #endregion
        }
        #endregion

        #region ButtonA Controls
        if (!buttonA_GCDOn && !buttonAPressed)
        {
            if (Input.GetButton("P" + player + " Button A"))
            {
                if (selectingCharacter)
                {
                    selectingCharacter = false;
                    selectingSpells = true;
                    selectedButtonGameObject.SetActive(true);
                    SelectionTintChange(0);
                }
                else if (selectingSpells)
                {
                    selectingSpells = false;

                    if (selectedButton < 5)
                    {
                        foreach (KeyValuePair<string, Spell> entry in spells)
                        {
                            switch (selectedButton)
                            {
                                case 0:
                                    if (entry.Value.spellObject.name == "Spell_LB")
                                    {
                                        choosingSpell = entry.Key;
                                        //Destroy(entry.Value);
                                    }
                                    break;
                                case 1:
                                    if (entry.Value.spellObject.name == "Spell_LT")
                                    {
                                        choosingSpell = entry.Key;
                                        //Destroy(entry.Value);
                                    }
                                    break;
                                case 2:
                                    if (entry.Value.spellObject.name == "Spell_RT")
                                    {
                                        choosingSpell = entry.Key;
                                        //Destroy(entry.Value);
                                    }
                                    break;
                                case 3:
                                    if (entry.Value.spellObject.name == "Spell_RB")
                                    {
                                        choosingSpell = entry.Key;
                                        //Destroy(entry.Value);
                                    }
                                    break;
                                case 4:
                                    if (entry.Value.spellObject.name == "Passive")
                                    {
                                        choosingSpell = entry.Key;
                                        //Destroy(entry.Value);
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }

                        if (selectedButton < 4)
                        {
                            //Debug.Log(choosingSpell);
                            //spells.Remove(choosingSpell);
                            selectingSingleSpell = true;
                            RedSquarCtrl();
                            selectableSpellsGameObject.SetActive(true);
                        }
                        if (selectedButton == 4)
                        {
                            //spells.Remove(choosingSpell);
                            selectingSingleSpell = true;
                            RedSquarCtrl();
                            selectablePassivesGameObject.SetActive(true);
                        }
                    }
                    else if (selectedButton == 5)
                    {
                        playerReady = true;
                        Debug.Log("Player " + player + " ready!");
                        thisPlayerManager.PlayerReady(player);
                    }
                }
                else if (selectingSingleSpell)
                {
                    SelectSpell();

                    selectingSingleSpell = false;
                    selectingSpells = true;
                    selectableSpellsGameObject.SetActive(false);
                    selectablePassivesGameObject.SetActive(false);
                }

                buttonAPressed = true;
            }
        }
        else if (Input.GetButtonUp("P" + player + " Button A") && buttonAPressed)
        {
            buttonAPressed = false;
        }

        #endregion

        #region ButtonB Controls
        if (!buttonB_GCDOn && !buttonBPressed)
        {
            if (Input.GetButton("P" + player + " Button B"))
            {
                if (selectingCharacter)
                {
                    //Deactivate Player
                    selectingCharacter = false;

                    thisPlayerManager.Deactivate(player);
                }
                else if (selectingSpells)
                {
                    //Back to Selecting Character
                    selectingSpells = false;
                    selectingCharacter = true;
                    selectedButton = 0;
                    SelectionTintChange(0);
                    selectedButtonGameObject.SetActive(false);
                }
                else if (selectingSingleSpell)
                {
                    if (selectedButton <= 3)
                    {
                        //Back to Selecting spells
                        //SelectSpell();

                        selectingSingleSpell = false;
                        selectingSpells = true;
                        selectableSpellsGameObject.SetActive(false);
                    }
                    else if (selectedButton == 4)
                    {
                        //Back to Selecting spells
                        //SelectSpell();

                        selectingSingleSpell = false;
                        selectingPassive = false;
                        selectingSpells = true;
                        selectablePassivesGameObject.SetActive(false);
                    }
                }
                else if (playerReady)
                {
                    //Set Player Unready
                    playerReady = false;
                    thisPlayerManager.PlayerNotReady(player);
                }

                buttonBPressed = true;
            }
        }
        else if (Input.GetButtonUp("P" + player + " Button B") && buttonBPressed)
        {
            buttonBPressed = false;
        }
        #endregion
    }

    private void CharSelChanged()
    {
        Destroy(Character);
        Character = Instantiate(Resources.Load("Character/Character" + selectedCharacter), thisPlayer.transform.position, thisPlayer.transform.rotation) as GameObject;
        Character.name = Character.name.Remove(10);
        Character.transform.SetParent(thisPlayerRotationObject.transform);
        Character.transform.localPosition = new Vector3(0, 0, 0);
        thisPlayer.GetComponent<CharController>().character = selectedCharacter;

        foreach (KeyValuePair<string, Spell> entry in spells)
        {
            Destroy(entry.Value.spellObject);
        }

        spells.Clear();

        int spellMod = 0;
        switch (selectedCharacter)
        {
            case 1:
                spellMod = 0;
                break;
            case 2:
                spellMod = 4;
                break;
            case 3:
                spellMod = 8;
                break;
            case 4:
                spellMod = 12;
                break;
            default:
                break;
        }

        for (int j = 1; j <= 4; j++)
        {
            spells["Spell" + (spellMod + j)] = new Spell(GameObject.Instantiate(Resources.Load("Spells/Prefabs/Spell" + (spellMod + j))) as GameObject);
        }
        spells["Passive" + (selectedCharacter)] = new Spell(GameObject.Instantiate(Resources.Load("Spells/Prefabs/Passive" + selectedCharacter)) as GameObject);

        int i = 0;
        foreach (KeyValuePair<string, Spell> entry in spells)
        {
            entry.Value.spellObject.transform.SetParent(thisPlayerRotationObject.transform, false);

            switch (i)
            {
                case 0:
                    entry.Value.spellObject.name = "Spell_LB";
                    break;
                case 1:
                    entry.Value.spellObject.name = "Spell_LT";
                    break;
                case 2:
                    entry.Value.spellObject.name = "Spell_RT";
                    break;
                case 3:
                    entry.Value.spellObject.name = "Spell_RB";
                    break;
                case 4:
                    entry.Value.spellObject.name = "Passive";
                    break;
                default:
                    break;
            }
            selectableButtons[i].GetComponent<Image>().sprite = Resources.Load<Sprite>("Spells/Sprites/Icon_" + entry.Key);
            i++;
        }

        PHC = Character.transform.root.GetComponent<PlayerHealthController>();
        PHC.healthObject = Character.transform.Find("Health").gameObject;
        PHC.AddLifePartsToList();
    }

    private void SelectionTintChange(int character)
    {
        for (int i = 0; i < sSpellTints.Count; i++)
        {
            if (i != character)
            {
                sSpellTints[i].SetActive(true);
            }
            if (i == character)
            {
                sSpellTints[i].SetActive(false);
            }
        }
    }

    #region GCD IEnumerators
    //Need them for the GCD
    private IEnumerator Thumbstick_GCD()
    {
        thumbstick_GCDOn = true;
        yield return thumbstick_GCD;
        thumbstick_GCDOn = false;
    }

    private IEnumerator ButtonA_GCD()
    {
        buttonA_GCDOn = true;
        yield return buttonA_GCD;
        buttonA_GCDOn = false;
    }

    private IEnumerator ButtonB_GCD()
    {
        buttonA_GCDOn = true;
        yield return buttonB_GCD;
        buttonB_GCDOn = false;
    }
    #endregion

    private void RedSquarCtrl()
    {
        if (selectedButton <= 3)
        {
            float chosenX = 0;
            float chosenY = 0;

            #region Switch between the Spells
            int temp = int.Parse(choosingSpell.Remove(0, 5));
            switch (temp)
            {
                case 1:
                    chosenX = -70;
                    chosenY = -250;
                    break;
                case 2:
                    chosenX = -22.5f;
                    chosenY = -250;
                    break;
                case 3:
                    chosenX = 22.5f;
                    chosenY = -250;
                    break;
                case 4:
                    chosenX = 70;
                    chosenY = -250;
                    break;
                case 5:
                    chosenX = -70;
                    chosenY = -290;
                    break;
                case 6:
                    chosenX = -22.5f;
                    chosenY = -290;
                    break;
                case 7:
                    chosenX = 22.5f;
                    chosenY = -290;
                    break;
                case 8:
                    chosenX = 70;
                    chosenY = -290;
                    break;
                case 9:
                    chosenX = -70;
                    chosenY = -330;
                    break;
                case 10:
                    chosenX = -22.5f;
                    chosenY = -330;
                    break;
                case 11:
                    chosenX = 22.5f;
                    chosenY = -330;
                    break;
                case 12:
                    chosenX = 70;
                    chosenY = -330;
                    break;
                case 13:
                    chosenX = -70;
                    chosenY = -370;
                    break;
                case 14:
                    chosenX = -22.5f;
                    chosenY = -370;
                    break;
                case 15:
                    chosenX = 22.5f;
                    chosenY = -370;
                    break;
                case 16:
                    chosenX = 70;
                    chosenY = -370;
                    break;
                default:
                    Debug.Log("what");
                    break;
            }
            #endregion

            selectionBorderSpellsGameObject.transform.localPosition = new Vector2(chosenX, chosenY);
        }
        else if (selectedButton == 4)
        {
            float chosenX = 88;
            float chosenY = 0;

            int temp = int.Parse(choosingSpell.Remove(0, 7));
            switch (temp)
            {
                case 1:
                    chosenY = -250f;
                    break;
                case 2:
                    chosenY = -290f;
                    break;
                case 3:
                    chosenY = -330f;
                    break;
                case 4:
                    chosenY = -370f;
                    break;
                default:
                    break;
            }

            selectionBorderPassivesGameObject.transform.localPosition = new Vector2(chosenX, chosenY);
        }
    }

    private void SelectSpell()
    {
        if (!spells.ContainsKey(choosingSpell))
        {
            foreach (KeyValuePair<string, Spell> entry in spells)
            {
                string temp;
                switch (selectedButton)
                {
                    case 0:
                        temp = "Spell_LB";
                        break;
                    case 1:
                        temp = "Spell_LT";
                        break;
                    case 2:
                        temp = "Spell_RT";
                        break;
                    case 3:
                        temp = "Spell_RB";
                        break;
                    case 4:
                        temp = "Passive";
                        break;
                    default:
                        temp = "";
                        break;
                }

                if (entry.Value.spellObject.name == temp)
                {
                    Destroy(entry.Value.spellObject);
                    spells.Remove(entry.Key);
                    break;
                }
            }

            spells[choosingSpell] = new Spell(GameObject.Instantiate(Resources.Load("Spells/Prefabs/" + choosingSpell)) as GameObject);

            spells[choosingSpell].spellObject.transform.SetParent(thisPlayerRotationObject.transform, false);

            selectableButtons[selectedButton].GetComponent<Image>().sprite = Resources.Load<Sprite>("Spells/Sprites/Icon_" + choosingSpell);

            switch (selectedButton)
            {
                case 0:
                    spells[choosingSpell].spellObject.name = "Spell_LB";
                    return;
                case 1:
                    spells[choosingSpell].spellObject.name = "Spell_LT";
                    return;
                case 2:
                    spells[choosingSpell].spellObject.name = "Spell_RT";
                    return;
                case 3:
                    spells[choosingSpell].spellObject.name = "Spell_RB";
                    return;
                case 4:
                    spells[choosingSpell].spellObject.name = "Passive";
                    return;
                default:
                    break;
            }
        }
        else if (spells.ContainsKey(choosingSpell))
        {
            string currentSpell = selectableButtons[selectedButton].GetComponent<Image>().sprite.name.Remove(0, 5);
            string tempName = spells[currentSpell].spellObject.name;

            spells[currentSpell].spellObject.name = spells[choosingSpell].spellObject.name;
            spells[choosingSpell].spellObject.name = tempName;

            for (int i = 0; i <= 4; i++)
            {
                if (selectableButtons[i].GetComponent<Image>().sprite.name.Remove(0, 5) == choosingSpell)
                {
                    selectableButtons[i].GetComponent<Image>().sprite = Resources.Load<Sprite>("Spells/Sprites/Icon_" + currentSpell);
                }
            }

            selectableButtons[selectedButton].GetComponent<Image>().sprite = Resources.Load<Sprite>("Spells/Sprites/Icon_" + choosingSpell);
        }
    }
}

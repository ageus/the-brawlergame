﻿using UnityEngine;
using System.Collections;

public class Stachel_Damage : MonoBehaviour
{
    public int StachelDMG;

    void OnCollisionStay2D(Collision2D col)
    {
        PlayerHealthController phc;
        if ((phc = col.gameObject.GetComponent<PlayerHealthController>()) != null)
        {
            if (!phc.untargetable)
            {
                phc.AdjustHealth(StachelDMG);
            }
        }
    }
}

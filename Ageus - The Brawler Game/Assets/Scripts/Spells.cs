﻿using UnityEngine;
using System.Collections;

public class Spells : MonoBehaviour {

    public string spellName;
    public float damage;
    public float cooldown;
    public int casterID;
    public float speed;

    public override string ToString()
    {
        return "Spell: " + name + " was used, and dealt " + damage + " damage!";
    }
}

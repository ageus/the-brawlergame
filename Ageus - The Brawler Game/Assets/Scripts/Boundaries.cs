﻿using UnityEngine;
using System.Collections;

public class Boundaries : MonoBehaviour
{

    public bool isRight = true;
    public Transform otherBoundary;

    void OnTriggerEnter2D(Collider2D otherCol)
    {
        print(otherCol.gameObject.name);
        if (otherCol.tag == "MovementCollider")
        {
            float otherBPos = otherBoundary.position.x;

            if (isRight)
            {
                otherBPos += 10f;
            }
            else
            {
                otherBPos -= 10f;
            }

            otherCol.gameObject.transform.root.transform.position = new Vector2(otherBPos, otherCol.transform.position.y);
        }
        else if (otherCol.tag == "Projectile")
        {
            float otherBPos = otherBoundary.position.x;

            if (isRight)
            {
                otherBPos += 10f;
            }
            else
            {
                otherBPos -= 10f;
            }

            otherCol.gameObject.transform.position = new Vector2(otherBPos, otherCol.transform.position.y);
        }
    }
}

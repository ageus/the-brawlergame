﻿using UnityEngine;
using System.Collections;

public class Spell_Grab : MonoBehaviour
{

    public int casterID;

    public GameObject grabPrefab;
    public float timeToKill;
    public float horizontalForce;
    public float verticalForce;

    private GameObject go;
    private bool spawned = false;
    private float aimDirX = 0;
    private float aimDirY = 0;
    private CharController cc;
    private bool shot = false;

    public string prefabName;

    void Go()
    {
        go = Instantiate(grabPrefab, this.transform.position, this.transform.root.rotation) as GameObject;
        go.GetComponent<Spell_Grab_CollisionDetection>().buttonName = this.name;
        go.GetComponent<Spell_Grab_CollisionDetection>().casterID = casterID;
        go.GetComponent<Spell_Grab_CollisionDetection>().Sender = this.gameObject;
        go.transform.SetParent(transform.root.transform);
        cc = transform.root.GetComponent<CharController>();
        casterID = cc.player;
        spawned = true;
    }
    void FixedUpdate()
    {
        if (spawned)
        {
            if (cc.aimDirectionDegr == 0)
            {
                aimDirX = 5 * -transform.parent.localScale.z;
                aimDirY = 0;
            }
            else if (cc.aimDirectionDegr == 45)
            {
                aimDirX = 3f * -transform.parent.localScale.z;
                aimDirY = 3f;
            }
            else if (cc.aimDirectionDegr == 90)
            {
                aimDirX = 0;
                aimDirY = 5;
            }
            else if (cc.aimDirectionDegr == 315)
            {
                aimDirX = 3 * -transform.parent.localScale.z;
                aimDirY = -3;
            }
            else if (cc.aimDirectionDegr == 270)
            {
                aimDirX = 0;
                aimDirY = -5;
            }
            go.transform.localPosition = new Vector2(aimDirX, aimDirY);
        }
        if (shot)
        {
            go.GetComponent<LineRenderer>().SetPosition(1, go.transform.position);
            go.GetComponent<LineRenderer>().SetPosition(0, transform.root.position);
        }
    }
    void Stop()
    {
        shot = true;
        spawned = false;
        go.GetComponent<Rigidbody2D>().velocity = new Vector2(aimDirX * horizontalForce, aimDirY * verticalForce);
        StartCoroutine(IEKill());
    }
    public IEnumerator IEKill()
    {
        yield return new WaitForSeconds(timeToKill);
        shot = false;
        Destroy(go);
    }
}

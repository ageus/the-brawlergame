﻿using UnityEngine;
using System.Collections;

public class Spell_Versteinerung : MonoBehaviour {

    public float casterID;
    public float duration;
    public float coolDown;
    public bool castable = true;

    private SpriteRenderer sprite;

    private CharController cc;

    public string prefabName;
    
    public void Start()
    {
        cc = transform.root.GetComponent<CharController>();
        casterID = cc.player;
        sprite = GetComponentInChildren<SpriteRenderer>();
    }

    public void Go()
    {
        if (castable)
        {
            sprite.enabled = true;
            cc.blockedInputs = true;
            cc.targetable = false;
            castable = false;
            StartCoroutine(timer(duration));
        }
        else
        {
            Debug.Log("Versteinerung is on CD");
        }
    }

    IEnumerator timer(float waitfor)
    {
        yield return new WaitForSeconds(waitfor);
        if (waitfor == duration)
        {
            cc.targetable = true;
            cc.blockedInputs = false;
            StartCoroutine(timer(coolDown));
            sprite.enabled = false;
        }
        else if(waitfor == coolDown)
        {
            castable = true;
        }
    }

    public void Stop()
    {

    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Passive_Schleim : MonoBehaviour
{
    [Header("Debug")]
    public int casterID;
    public List<int> affectedPlayers = new List<int>();

    [Header("Balancing")]
    public float callfrequenz = 0.125f;  //called every X seconds
    public float fadeStep = 0.05f;      //fades X every step
    public float speedUpAmount = 5;     //casters speed rises for X
    public float slowAmount = 2.5f;     //enemies speed falls for X

    public GameObject slimePrefab;

    private CharController cc;

    void Start()
    {
        cc = transform.root.GetComponent<CharController>();
        casterID = cc.player;
        StartCoroutine(SpawnSlime());
    }

    private IEnumerator SpawnSlime()
    {
        while (true)
        {
            yield return new WaitForSeconds(callfrequenz);
            if (!cc.blockedInputs && cc.isGrounded)
            {
            //if (!cc.blockedInputs)    //WENN AUCH IN LUFT SPAWN ENABLED SEIN SOLL
            //{
                GameObject go = Instantiate(slimePrefab, transform.position, transform.rotation) as GameObject;
                Slime slime = new Slime(go, fadeStep, casterID, speedUpAmount, slowAmount, this);
                StartCoroutine(slime.Vanish());
            }
        }
    }
}

public class Slime
{
    public GameObject sprite;
    private float fadeStep;

    public Slime(GameObject _sprite, float _fadeStep, int _casterID, float _speedUpAmount, float _slowAmount, Passive_Schleim _script)
    {
        fadeStep = _fadeStep;
        sprite = _sprite;
        Passive_SchleimCollisionDetection temp = sprite.GetComponent<Passive_SchleimCollisionDetection>();
        temp.casterID = _casterID;
        temp.SlowAmount = _slowAmount;
        temp.SpeedUpAmount = _speedUpAmount;
        temp.Sender = _script;
    }

    public IEnumerator Vanish()
    {
        SpriteRenderer spriterenderer = sprite.GetComponent<SpriteRenderer>();
        for (float f = 1f; f >= 0; f -= fadeStep)
        {
            Color c = spriterenderer.material.color;
            c.a = f;
            spriterenderer.material.color = c;
            yield return new WaitForSeconds(0.1f);
        }
        GameObject.Destroy(sprite);
    }
}

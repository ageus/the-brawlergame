﻿using UnityEngine;
using System.Collections;

public class Spell_Bomb : MonoBehaviour
{
    public int casterID;
    public int damage;
    public float coolDown;
    public GameObject bombPrefab;
    public float howbouncy;
    public float howfriction;
    public float explosionRange;
    public LayerMask PlayerLayer;
    public int maxBounceTimes;

    public float timeToKillValue;
    public int bounceTimes = 0;

    private Collider2D[] playersHit;
    private GameObject go;
    private bool spawned;
    private float aimDirX = 0;
    private float aimDirY = 0;
    private CharController cc;
    public float horizontalForce;
    public float verticalForce;
    public bool shot;
    private bool shootable = true;

    public string prefabName;

    void Go()
    {
        if (shootable)
        {
            go = Instantiate(bombPrefab, this.transform.position, this.transform.root.rotation) as GameObject;
            go.GetComponent<Spell_BombCollisionDetection>().parentScript = GetComponent<Spell_Bomb>();
            go.transform.SetParent(transform.root.transform);
            go.GetComponent<Collider2D>().enabled = false;
            go.GetComponent<Rigidbody2D>().isKinematic = true;
            cc = transform.root.GetComponent<CharController>();
            go.GetComponent<Collider2D>().sharedMaterial.bounciness = howbouncy;
            go.GetComponent<Collider2D>().sharedMaterial.friction = howfriction;
            casterID = cc.player;
            spawned = true;
            shot = false;
            shootable = false;
        }
    }

    void Update()
    {
        if (spawned && !shot)
        {
            if (cc.aimDirectionDegr == 0)
            {
                aimDirX = 3 * -transform.parent.localScale.z;
                aimDirY = 3;
            }
            else if (cc.aimDirectionDegr == 45)
            {
                aimDirX = 3f * -transform.parent.localScale.z;
                aimDirY = 3f;
            }
            else if (cc.aimDirectionDegr == 90)
            {
                aimDirX = 0;
                aimDirY = 5;
            }
            else if (cc.aimDirectionDegr == 315)
            {
                aimDirX = 3 * -transform.parent.localScale.z;
                aimDirY = 3;
            }
            else if (cc.aimDirectionDegr == 270)
            {
                aimDirX = 3;
                aimDirY = 3;
            }
            go.transform.localPosition = new Vector2(aimDirX, aimDirY);
        }
    }

    void Stop()
    {
        if (!shot && go != null)
        {
            go.GetComponent<Collider2D>().enabled = true;
            go.GetComponent<Rigidbody2D>().isKinematic = false;
            go.GetComponent<Rigidbody2D>().velocity = new Vector2(aimDirX * horizontalForce, aimDirY * verticalForce);
            spawned = false;
            shot = true;
            StartCoroutine(TimeToKill(timeToKillValue));
            StartCoroutine(CoolDown(coolDown));
        }
    }

    IEnumerator CoolDown(float waitfor)
    {
        yield return new WaitForSeconds(waitfor);
        shootable = true;
    }

    IEnumerator TimeToKill(float waitfor)
    {
        yield return new WaitForSeconds(waitfor);
        Explode();
    }

    public void Explode()
    {
        //StopCoroutine(TimeToKill(timeToKillValue));
        shot = false;
        bounceTimes = 0;
        playersHit = Physics2D.OverlapCircleAll(go.transform.position, explosionRange, PlayerLayer);
        foreach (Collider2D col in playersHit)
        {
            //DEAL DAMAGE
            //Debug.Log(col.gameObject.name + " has been hit by an explosion!");
            col.transform.root.GetComponent<PlayerHealthController>().AdjustHealth(-damage);
        }
        Destroy(go);
    }

}

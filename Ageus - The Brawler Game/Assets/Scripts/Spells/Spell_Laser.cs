﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spell_Laser : MonoBehaviour
{

    public int casterID;
    public float range;
    public int dmg;
    public float duration;
    public float cooldown;
    private RaycastHit2D[] hit;

    public LayerMask hitableLayers;

    private CharController cc;
    private LineRenderer line;
    private Rigidbody2D body;

    private bool triggered = false;
    private bool onCD = false;
    private Vector3 lineStartPosition;
    private Vector3 lineEndPosition;

    private float aimDirX = 0;
    private float aimDirY = 0;

    private List<int> notDamageablePlayerIDs = new List<int>();

    void Start()
    {
        cc = transform.root.GetComponent<CharController>();
        body = transform.root.GetComponent<Rigidbody2D>();
        line = GetComponent<LineRenderer>();
        casterID = cc.player;
    }

    void Go()
    {
        if (!onCD)
        {
            cc.blockedMovement = true;
            triggered = true;
            body.velocity = new Vector2(0, 0);
            line.enabled = true;
            StartCoroutine(End(duration));
        }
    }

    void FixedUpdate()
    {
        if (triggered)
        {
            float horizontalAim = Input.GetAxis("P" + casterID + "L Horizontal");
            if (horizontalAim <= 0)
            {
                transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, 1);
            }
            else
            {
                transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, -1);
            }
            Ray2D ray = new Ray2D(transform.position, new Vector2(horizontalAim, -Input.GetAxis("P" + casterID + "L Vertical")));
            hit = Physics2D.RaycastAll(ray.origin, ray.direction, range, hitableLayers);
            if (hit.Length > 1)
            {
                line.SetPosition(0, transform.position);
                CharController hitcc;
                PlayerHealthController hitphc;
                for (int i = 1; i < hit.Length; i++)
                {
                    if ((hitcc = hit[i].transform.GetComponent<CharController>()) == null)// || hitcc.player == casterID)
                    {
                        Debug.Log("Has hit a Platform");
                        line.SetPosition(1, hit[i].point);
                        return;
                    }
                    else if(notDamageablePlayerIDs.Contains(hitcc.player))
                    {
                        hitphc = hit[i].transform.root.GetComponent<PlayerHealthController>();
                        StartCoroutine(DamagedPlayerCD(hitcc.player,hitphc));
                    }
                }
            }
            line.SetPosition(1, ray.GetPoint(range));

        }
    }

    void Stop()
    {
        StopCoroutine(End(0));
        StartCoroutine(End(0));
    }

    IEnumerator End(float time)
    {
        yield return new WaitForSeconds(time);
        triggered = false;
        line.enabled = false;
        onCD = true;
        cc.blockedMovement = false;
        StartCoroutine(CoolDown());
    }

    IEnumerator DamagedPlayerCD(int hitID, PlayerHealthController phc)
    {
        notDamageablePlayerIDs.Add(hitID);
        phc.AdjustHealth(-dmg);
        yield return new WaitForSeconds(0.5f);
        notDamageablePlayerIDs.Remove(hitID);
    }

    IEnumerator CoolDown()
    {
        yield return new WaitForSeconds(cooldown);
        onCD = false;
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Passive_SchleimCollisionDetection : MonoBehaviour
{

    public int casterID;
    private float slowAmount;
    private float speedUpAmount;
    private Passive_Schleim sender;


    public float SlowAmount
    {
        get
        {
            return slowAmount;
        }
        set
        {
            slowAmount = value;
        }
    }
    public float SpeedUpAmount
    {
        get
        {
            return speedUpAmount;
        }
        set
        {
            speedUpAmount = value;
        }
    }
    public Passive_Schleim Sender
    {
        get
        {
            return sender;
        }
        set
        {
            sender = value;
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        //Debug.Log("Has Collided!");
        try
        {
            CharController cc = col.transform.root.GetComponent<CharController>();
            if (!sender.affectedPlayers.Contains(cc.player))
            {
                if (cc.player != casterID)
                {
                    cc.maxSpeed -= slowAmount;
                }
                else if (cc.player == casterID)
                {
                    cc.maxSpeed += speedUpAmount;
                }
                sender.affectedPlayers.Add(cc.player);
            }
        }
        catch (System.NullReferenceException nex)
        {
            Debug.Log("NullReferebceException: " + nex.Message);
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        try
        {
            CharController cc = col.transform.root.GetComponent<CharController>();
            if (sender.affectedPlayers.Contains(cc.player))
            {
                if (cc.player != casterID)
                {
                    cc.maxSpeed += slowAmount;
                    sender.affectedPlayers.Remove(cc.player);
                }
                else if (cc.player == casterID)
                {
                   // cc.maxSpeed -= speedUpAmount;
                }
                //sender.affectedPlayers.Remove(cc.player);
            }
        }
        catch (System.NullReferenceException nex)
        {
            Debug.Log("NullReferebceException: " + nex.Message);
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class Spell_Double : MonoBehaviour {

    private int casterID;
    private GameObject spawnedDouble;
    private bool hasSpawned = false;
    private CharController cc;
    private bool OnCD = false;

    public float CoolDownTime = 20;
    public float timeToKill = 5;

    void Start()
    {
        cc = transform.root.GetComponent<CharController>();
        casterID = cc.player;
    }

    void Go()
    {
        if (!hasSpawned && !OnCD)
        {
            if (spawnedDouble != null)
            {
                Destroy(spawnedDouble);
            }
            spawnedDouble = Instantiate(Resources.Load("Character/Character" + cc.character), transform.position, transform.rotation) as GameObject;
                                   
            foreach (Transform child in spawnedDouble.transform)
            {
                if (child.name == "MovementCol")
                {
                    child.GetComponent<BoxCollider2D>().enabled = false;
                }
            }

            hasSpawned = true;
            OnCD = true;
            StartCoroutine(Kill());
            StartCoroutine(CoolDown());
        }
        else if(hasSpawned)
        {
            Vector3 tempPos = transform.root.position;
            transform.root.position = spawnedDouble.transform.position;
            spawnedDouble.transform.position = tempPos;
        }
    }

    void Stop()
    {

    }

    IEnumerator CoolDown()
    {
        yield return new WaitForSeconds(CoolDownTime);
        OnCD = false;
    }

    IEnumerator Kill()
    {
        yield return new WaitForSeconds(timeToKill);
        Destroy(spawnedDouble);
        hasSpawned = false;
    }
}

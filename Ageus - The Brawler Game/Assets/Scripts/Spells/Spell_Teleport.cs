﻿using UnityEngine;
using System.Collections;

public class Spell_Teleport : MonoBehaviour
{
    public int casterID;
    public float cooldown;
    public float damage;
    public float channelTime;
    public float range;

    private bool castAble = true;

    public string prefabName;

    void Update()
    {
        //if (casterID > 0)
        //{
        //    if (Input.GetButtonDown("P" + casterID + " Button Y"))
        //    {
        //        CastSpell();
        //    }
        //}
        //else
        //{
        //    casterID = GetComponent<CharController>().player;
        //}
    }

    void Start()
    {
        casterID = transform.root.GetComponent<CharController>().player;
    }

    void Go()
    {
        if (!castAble)
            return;

        float axeHorizontal = Input.GetAxis("P" + casterID + "L Horizontal");
        float axeVertical = -Input.GetAxis("P" + casterID + "L Vertical");

        if (axeVertical == 0 && axeHorizontal == 0)
            return;

        Vector3 newPosition = new Vector3(axeHorizontal * range
                                        , axeVertical * range
                                        , 0);
        newPosition = transform.root.transform.position + newPosition;

        RaycastHit2D hit;
        hit = Physics2D.Linecast(new Vector2(transform.root.transform.position.x, transform.root.transform.position.y), new Vector2(newPosition.x, newPosition.y), 1 << LayerMask.NameToLayer("Platform"));

        if (hit == null ||
            hit.transform == null ||
            hit.transform.tag != "Rand")
        {
            transform.root.transform.position = newPosition;

            castAble = false;
            StartCoroutine(IECoolDown());

            Debug.Log("Success: " + newPosition.x + "/" + newPosition.y);
        }
        else
        {
            if (hit.distance < range)
            {
                transform.root.GetComponent<CharController>().blockedInputs = true;
                transform.root.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                transform.root.GetComponent<Rigidbody2D>().angularVelocity = 0;

                Vector2 telDistance = hit.point - new Vector2(transform.position.x, transform.position.y);

                newPosition = new Vector3(axeHorizontal * (Mathf.Abs(telDistance.x) - transform.root.GetComponent<Collider2D>().bounds.size.x / 2),
                                          axeVertical * (Mathf.Abs(telDistance.y) - transform.root.GetComponent<Collider2D>().bounds.size.y / 2), 
                                          0);
                transform.root.transform.position += newPosition;

                castAble = false;
                StartCoroutine(IECoolDown());
                transform.root.GetComponent<CharController>().blockedInputs = false;
            }
        }
    }

    void Stop()
    {

    }

    IEnumerator IECoolDown()
    {
        yield return new WaitForSeconds(cooldown);
        castAble = true;
    }

}

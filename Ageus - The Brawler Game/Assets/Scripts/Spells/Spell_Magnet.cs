﻿using UnityEngine;
using System.Collections;

public class Spell_Magnet : MonoBehaviour
{

    public int casterID;
    public int cooldown;
    private bool onCD = false;

    public GameObject magnetPrefab;
    public float forceMultiplier;
    public float timeToKill;
    public float horizontalForce;
    public float verticalForce;

    private GameObject go;
    private bool spawned;
    private float aimDirX = 0;
    private float aimDirY = 0;
    private CharController cc;

    public string prefabName;
    public GameObject AnimationControllerMagnet;

    void Go()
    {
        if (onCD)
        {
            return;
        }
        go = Instantiate(magnetPrefab, this.transform.position, this.transform.root.rotation) as GameObject;
        go.transform.SetParent(transform.root.transform);
        AnimationControllerMagnet = go.transform.Find("AnimationControllerMagnet").gameObject;
        cc = transform.root.GetComponent<CharController>();
        casterID = cc.player;
        spawned = true;
    }
    void Update()
    {
        if (spawned)
        {
            if (cc.aimDirectionDegr == 0)
            {
                aimDirX = 5 * -transform.parent.localScale.z;
                aimDirY = 0;
            }
            else if (cc.aimDirectionDegr == 45)
            {
                aimDirX = 3f * -transform.parent.localScale.z;
                aimDirY = 3f;
            }
            else if (cc.aimDirectionDegr == 90)
            {
                aimDirX = 0;
                aimDirY = 5;
            }
            else if (cc.aimDirectionDegr == 315)
            {
                aimDirX = 3 * -transform.parent.localScale.z;
                aimDirY = -3;
            }
            else if (cc.aimDirectionDegr == 270)
            {
                aimDirX = 0;
                aimDirY = -5;
            }
            go.transform.localPosition = new Vector2(aimDirX, aimDirY);
        }
    }
    void Stop()
    {
        if (spawned)
        {
            AnimationControllerMagnet.SetActive(true);

            go.GetComponent<Rigidbody2D>().velocity = new Vector2(aimDirX * horizontalForce, aimDirY * verticalForce);
            go.transform.parent = null;
            go.GetComponent<PointEffector2D>().enabled = true;
            spawned = false;
            onCD = true;
            StartCoroutine(IEKill());
            StartCoroutine(IECoolDown());
        }
    }
    IEnumerator IEKill()
    {
        yield return new WaitForSeconds(timeToKill * 0.75f);
        go.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        yield return new WaitForSeconds(timeToKill * 5);
        Destroy(go);
    }
    IEnumerator IECoolDown()
    {
        yield return new WaitForSeconds(timeToKill * 0.75f + timeToKill * 5f + cooldown);
        onCD = false;        
    }
}

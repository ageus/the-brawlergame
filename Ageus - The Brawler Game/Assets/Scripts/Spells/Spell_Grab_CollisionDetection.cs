﻿using UnityEngine;
using System.Collections;

public class Spell_Grab_CollisionDetection : MonoBehaviour
{
    public float casterID;
    public float stunDuration;
    public float maxRange;

    public string buttonName = "";
    public GameObject Sender;

    private CharController hitenemy;
    private bool hasHit = false;
    private bool gcd = false;
    private bool hookUp = false;

    void Start()
    {
        //casterID = transform.root.GetComponent<CharController>().player;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.transform.root.tag == "Player")
        {
            Debug.Log("Has hit a player");
            CharController hitenemy = col.transform.root.gameObject.GetComponent<CharController>();
            if (hitenemy.player != casterID)
            {
                StopCoroutine(Sender.GetComponent<Spell_Grab>().IEKill());
                Sender.GetComponent<LineRenderer>().SetColors(Color.red, Color.yellow);
                hasHit = true;
                hitenemy.blockedInputs = true;
                StartCoroutine(IEStun());
            }
        }
    }

    void FixedUpdate()
    {
        if (hasHit)
        {

            string[] namearray = buttonName.Split('_');
            if (namearray[1] == "RT" || namearray[1] == "LT")
            {
                if (Input.GetAxis("P" + casterID + namearray[1]) > 0.5 && !gcd)
                {
                    gcd = true;
                }
            }
            else if (namearray[1] == "RB" || namearray[1] == "LB")
            {
                if (Input.GetButton("P" + casterID + namearray[1]) && !gcd)
                {
                    gcd = true;
                }
            }
        }
        if (gcd)
        {
            hitenemy.transform.position = Vector2.MoveTowards(this.transform.position ,transform.root.position, maxRange);
        }
    }

    IEnumerator IEStun()
    {
        yield return new WaitForSeconds(stunDuration);
        hitenemy.blockedInputs = false;
    }
}

﻿using UnityEngine;
using System.Collections;

public class Spell_Buff : MonoBehaviour {

    public float coolDown;
    public float duration;
    public float multiplier;
    public GameObject particleObj;

    private float normalSpeed;

    private bool canCast = true;
    private int casterID;

    public string prefabName;

    void Go()
    {
        if(!canCast)
            return;
        normalSpeed = this.transform.root.GetComponent<CharController>().maxSpeed;
        this.transform.root.GetComponent<CharController>().maxSpeed *= multiplier;
        canCast = false;
        particleObj.SetActive(true);
        StartCoroutine(IEDuration());
    }

    void Stop()
    {
        StopCoroutine(IEDuration());
        StartCoroutine(IECoolDown());
        transform.root.GetComponent<CharController>().maxSpeed = normalSpeed;
        particleObj.SetActive(false);
    }

    IEnumerator IEDuration()
    {
        yield return new WaitForSeconds(duration);
        StartCoroutine(IECoolDown());
        this.transform.root.GetComponent<CharController>().maxSpeed = normalSpeed;
        particleObj.SetActive(false);
    }

    IEnumerator IECoolDown()
    {
        yield return new WaitForSeconds(coolDown);
        canCast = true;
    }
}

﻿using UnityEngine;
using System.Collections;

public class Passive_TrippleJump : MonoBehaviour
{

    public string prefabName;
    CharController cc;
    SpriteRenderer rend;

    void Start()
    {
        cc = transform.root.GetComponent<CharController>();
        rend = GetComponentInChildren<SpriteRenderer>();
        cc.TripleJumpPassiveActive = true;
        rend.enabled = true;
    }

    void FixedUpdate()
    {
        if (cc.isDoubleJumping && !cc.TripleJumped)
        {
            rend.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        }
        else if (cc.TripleJumped)
        {
            rend.enabled = false;
            rend.transform.localScale = new Vector3(1f, 1f, 1f);
        }
        if (cc.isGrounded)
        {
            rend.enabled = true; 
            rend.transform.localScale = new Vector3(1f, 1f, 1f);
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class Spell_BombCollisionDetection : MonoBehaviour
{

    public Spell_Bomb parentScript;


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Rand" &&  parentScript.shot)
        {
            parentScript.bounceTimes++;
            if (parentScript.bounceTimes >= parentScript.maxBounceTimes)
            {
                parentScript.Explode();
            }
        }
        if (other.tag == "Player" && other.transform.root.GetComponent<CharController>().player != parentScript.casterID)
        {
            if (other.GetComponent<CharController>().player != parentScript.casterID)
            {
                parentScript.Explode();
            }
        }

    }

}

﻿using UnityEngine;
using System.Collections;

public class Spell_Explosion : MonoBehaviour {

    public GameObject ExplosionPrefab;
    public int casterID;

    public float coolDown;
    public float timeToKill;
    public int damage;
    private bool boomable;
    private GameObject go;

    public LayerMask PlayerLayer;
    public float explosionRange;
    private Collider2D[] playersHit;


    public string prefabName;
    void Start()
    {
        casterID = transform.root.GetComponent<CharController>().player;
        boomable = true;
    }

    void Go()
    {
        if (boomable)
        {
            go = Instantiate(ExplosionPrefab, transform.position, transform.rotation) as GameObject;
            boomable = false;
            StartCoroutine(WaitFor(coolDown, true));
            StartCoroutine(WaitFor(timeToKill, false));

            playersHit = Physics2D.OverlapCircleAll(go.transform.position, explosionRange, PlayerLayer);
            foreach (Collider2D col in playersHit)
            {
                //DEAL DAMAGE
                //Debug.Log(col.gameObject.name + " has been hit by an explosion!");
                if (col.transform.root.GetComponent<CharController>().player != casterID)
                {
                    col.transform.root.GetComponent<PlayerHealthController>().AdjustHealth(-damage);
                }
            }
        }
    }

    void Stop()
    {

    }

    IEnumerator WaitFor(float waitfor, bool cd)
    {
        yield return new WaitForSeconds(waitfor);
        if (cd)
        {
            boomable = true;
        }
        else
        {
            Destroy(go);
        }
    }

}

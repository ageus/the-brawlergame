﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spell_Dash : MonoBehaviour
{
    public int casterID;
    public float cooldown;
    public float damage;
    public float channelTime;
    public float range;
    public float speed;

    private bool castAble = true;

    private Rigidbody2D myRigidbody;

    public string prefabName;

    private Vector3 posOld;
    private Vector3 posNew;
    private Transform childObject;
    public List<Sprite> spriteList = new List<Sprite>();

    void Start()
    {
        casterID = transform.root.GetComponent<CharController>().player;
        childObject = transform.FindChild("Sprite");
    }

    void Update()
    {
        //if (casterID > 0)
        //{
        //    if (Input.GetButtonDown("P" + casterID + " Button Y"))
        //    {
        //        CastSpell();
        //    }
        //}
        //else
        //{
        //    casterID = GetComponent<CharController>().player;
        //}
    }

    void Go()
    {
        if (!castAble)
            return;

        float axeHorizontal = Input.GetAxis("P" + casterID + "L Horizontal");

        if (axeHorizontal == 0)
            return;

        myRigidbody = transform.root.GetComponent<Rigidbody2D>();

        transform.root.GetComponent<CharController>().blockedInputs = true;

        posOld = transform.root.position;

        myRigidbody.constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
        myRigidbody.velocity = new Vector2(0, 0);
        myRigidbody.angularVelocity = 0;
        myRigidbody.velocity = new Vector2(axeHorizontal * speed, transform.position.y);
        castAble = false;
        StartCoroutine(IECastTime());
    }

    void Stop()
    {
        //this is needed no matter what!
    }

    IEnumerator IECastTime()
    {
        yield return new WaitForSeconds(channelTime);

        posNew = transform.root.position;
        childObject.localScale = new Vector3((posNew.x - posOld.x) / 10, 1, 1);
        Debug.Log(posNew.x - posOld.x);
        childObject.position = new Vector3(transform.root.position.x - ((posNew.x - posOld.x) / 2), transform.position.y, transform.root.transform.position.z);
        childObject.parent = null;
        childObject.GetComponent<SpriteRenderer>().sprite = spriteList[0];
        StartCoroutine(IESwapImage());

        transform.root.GetComponent<CharController>().blockedInputs = false;
        myRigidbody.velocity = new Vector2(0, 0);
        myRigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;
        StartCoroutine(IECoolDown());
    }

    IEnumerator IESwapImage()
    {
        childObject.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        childObject.GetComponent<SpriteRenderer>().sprite = spriteList[1];
        yield return new WaitForSeconds(0.2f);
        childObject.GetComponent<SpriteRenderer>().sprite = spriteList[2];
        yield return new WaitForSeconds(0.2f);
        childObject.GetComponent<SpriteRenderer>().sprite = spriteList[0];
        yield return new WaitForSeconds(0.2f);
        childObject.GetComponent<SpriteRenderer>().sprite = spriteList[1];
        yield return new WaitForSeconds(0.2f);
        childObject.GetComponent<SpriteRenderer>().sprite = spriteList[2];
        yield return new WaitForSeconds(0.2f);
        childObject.gameObject.SetActive(false);
        childObject.parent = this.transform;
    }

    IEnumerator IECoolDown()
    {
        yield return new WaitForSeconds(cooldown);
        castAble = true;
    }
}

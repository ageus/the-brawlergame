﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class PlayerHealthController : MonoBehaviour
{
    public int currentlife;
    public int maxLife = 11;
    public bool untargetable = false;
    //public string name;
    public GameObject healthObject;

    private double pieceValue;
    private CharController charController;
    private GameManager gameManager;

    public List<Transform> lifeParts = new List<Transform>();

    public void AddLifePartsToList()
    {
        lifeParts.Clear();

        foreach (Transform child in healthObject.transform)
        {
            if (child.name.Contains("Skull"))
            {
                child.gameObject.SetActive(false);
                lifeParts.Add(child);
            }
        }

        currentlife = maxLife;

        pieceValue = (double)maxLife / (double)lifeParts.Count;

        healthObject.SetActive(true);
        healthObject.transform.parent = healthObject.transform.root;
        charController = GetComponent<CharController>();
        gameManager = GameObject.FindGameObjectWithTag("PlayerManager").GetComponent<GameManager>();
    }

    public void AdjustHealth(int adjustment)
    {
        if (gameManager.Players[charController.player])
        {
            if (untargetable)
            {
                return;
            }

            int oldLife = currentlife;
            int adjustedLife = currentlife + adjustment;
            int showPieces = 0;
            if (adjustedLife == maxLife - 1)
            {
                showPieces = 1;
            }
            else if (adjustedLife < maxLife)
            {
                showPieces = (int)((double)lifeParts.Count - (double)pieceValue * (double)adjustedLife);
            }
            print(showPieces);
            showPieces--;

            if (adjustedLife <= maxLife && adjustedLife > 0)
            {
                if(adjustment < 0)
                {
                    charController.anim.SetTrigger("hurt");
                }

                for (int i = lifeParts.Count - 1; i >= showPieces + 1; i--)
                {
                    if (lifeParts[i].gameObject.activeInHierarchy)
                    {
                        lifeParts[i].gameObject.SetActive(false);
                    }
                }

                for (int i = 0; i <= showPieces; i++)
                {
                    if (!lifeParts[i].gameObject.activeInHierarchy && i <= lifeParts.Count - 2)
                    {
                        lifeParts[i].gameObject.SetActive(true);
                    }
                }

                currentlife += adjustment;
            }
            else if (adjustedLife <= 0)
            {
                for (int i = 0; i <= lifeParts.Count-1; i++)
                {
                    if (!lifeParts[i].gameObject.activeInHierarchy && i <= lifeParts.Count - 1)
                    {
                        lifeParts[i].gameObject.SetActive(true);
                    }
                }

                currentlife = 0;
                Death();
            }
            else
            {
                currentlife = maxLife;
            }

            untargetable = true;
            StartCoroutine(MakeTargetAble());
        }

    }

    public void Death()
    {
        //MUSS NOCH TESTEN OB GEWONNEN ETC.
        Debug.Log("Spieler " + charController.player + " ist gestorben!");
        gameManager.CheckForWin(charController.player);
        charController.blockedInputs = true;
        charController.anim.SetBool("dead", true);
    }

    public void OnBecameInvisible()
    {
        //SPIELER STIRBT
        Debug.Log("Spieler " + charController.player + " fiel aus der Welt!");
    }

    IEnumerator MakeTargetAble()
    {
        yield return new WaitForSeconds(0.5f);
        untargetable = false;
    }

    //IEnumerator SetHealthObjectInactive()
    //{
    //    yield return new WaitForSeconds(4);
    //    healthObject.SetActive(false);
    //}
}

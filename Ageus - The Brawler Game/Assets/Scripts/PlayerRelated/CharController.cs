﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharController : MonoBehaviour
{
    //BEI WALLJUMP JUMP EINFACH NOCHMAL ENABLEN!!

    [Header("Player Settings")]
    public GameObject characterObject;
    public int player = 0;
    public int character = 0;
    public float maxSpeed = 20F;
    //public float punchForce = 500f;
    public float jumpForce = 1000F;
    public float stompForce = 3000f;
    public float wallJumpMulti = 1.2f;
    //public float PunchCoolDown = 0.5f;
    public float ThrowCoolDown = 0.5f;
    public int aimDirectionDegr = 0;
    public WaitForSeconds button_GCD = new WaitForSeconds(0.2f);
    public Vector2 forceVector = new Vector2(0, 0);                 //Die Force die geaddet wenn man sich bewegt

    [Header("Required")]
    public LayerMask Platforms;
    public bool blockedInputs = true;
    public bool targetable = true;
    public bool blockedMovement = false;

    [Header("Spells")]
    public GameObject spell_LB;
    public GameObject spell_LT;
    public GameObject spell_RT;
    public GameObject spell_RB;

    [Header("Is ...")]
    public bool isGrounded = false;
    public bool isFacing = false;
    public bool isStomping = false;
    public bool isThrowing = false;
    public bool isSliding = false;
    public bool isDoubleJumping = false;
    public bool isJumpReleased = true;
    
    Transform groundCheck;
    Transform deepCheck1;
    Transform deepCheck2;
    Transform faceCheck;
    Transform sideCheck1;
    Transform sideCheck2;
    //Transform topCheck1;
    //Transform topCheck2;

    public bool facingRight = false;

    public Animator anim;

    GameObject holdenEnemy;
    Rigidbody2D holdenRigidbody;


    public bool TripleJumpPassiveActive = false;
    public bool TripleJumped = false;

    bool wallJumped = false;
    float timeSinceWallJump = 0;
    float cdWallJump = 1;
    int hasFaced = 0; //0_default | 1_right | 2_left

    //Size of Overlapcircles
    float groundRadius = 0.5f;
    public float faceRadius = 0.5f;
    float deepRadius = 2f;
    float sideRaius = 0.5f;

    Rigidbody2D myRigidbody;
    public GameObject myRotationObject;

    //Global Cooldowns
    bool buttonB_GCD = false;
    bool buttonX_GCD = false;
    bool buttonA_GCD = false;
    bool buttonLB_GCD = false;
    bool buttonRB_GCD = false;
    bool triggerLT_GCD = false;
    bool triggerRT_GCD = false;


    void Awake()
    {
        myRigidbody = GetComponent<Rigidbody2D>();

        myRotationObject = this.transform.Find("RotationObject").gameObject;
    }

    void Start()
    {
        characterObject = myRotationObject.transform.Find("Character" + character).gameObject;

        //These are on the Player gameobject
        groundCheck = transform.Find("Ground");
        deepCheck1 = transform.Find("Down1");
        deepCheck2 = transform.Find("Down2");

        //These are on the Character gameobject
        faceCheck = characterObject.transform.Find("Face");
        sideCheck1 = characterObject.transform.Find("Side1");
        sideCheck2 = characterObject.transform.Find("Side2");

        anim = characterObject.GetComponentInChildren<Animator>();
        anim.enabled = true;
    }

    void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, Platforms);
        isFacing = Physics2D.OverlapCircle(faceCheck.position, faceRadius, Platforms);

        //Animations
        anim.SetBool("grounded", isGrounded);
        anim.SetBool("facing", isFacing);

        if (blockedInputs)
            return;

        float axisL_Horizontal = Input.GetAxis("P" + player + "L Horizontal");

        if (isStomping)
        {
            CheckCollision(groundCheck, groundRadius, "stomp");
        }

        //check if grounded
        if (isGrounded)
        {
            isDoubleJumping = false;
            TripleJumped = false;
            hasFaced = 0;
            StopCoroutine(ButtonA_GCD());
            //wallJumped = false;
        }
        else if (!isGrounded && isFacing)
        {
            if (axisL_Horizontal < 0 && !facingRight)
            {
                axisL_Horizontal = 0;
            }
            else if (axisL_Horizontal > 0 && facingRight)
            {
                axisL_Horizontal = 0;
            }
        }

        #region Movement

        //MOVING
        //Change the velocity of the rigidbody by multiplying move with maxspeed and maintining Y velocity
        //if (!isThrowing && !isSliding)
        //{
        //    myRigidbody.velocity = new Vector2(axisL_Horizontal * maxSpeed, myRigidbody.velocity.y);
        //}
        //else if (isSliding && myRigidbody.velocity.x < 20 && myRigidbody.velocity.x > -20)
        //{
        //    myRigidbody.velocity += new Vector2(axisL_Horizontal * maxSpeed / 10, 0);
        //}
        //else if (isThrowing)
        //{
        //    myRigidbody.velocity = new Vector2(axisL_Horizontal * maxSpeed / 2, myRigidbody.velocity.y);
        //}
        //else
        //{
        //    myRigidbody.velocity = new Vector2(axisL_Horizontal * maxSpeed, myRigidbody.velocity.y);
        //}

        if (!isThrowing && !blockedMovement)
        {
            myRigidbody.velocity = new Vector2(axisL_Horizontal * maxSpeed, myRigidbody.velocity.y);
        }
        else if (isThrowing && !blockedMovement)
        {
            myRigidbody.velocity = new Vector2(axisL_Horizontal * maxSpeed / 2, myRigidbody.velocity.y);
        }
        myRigidbody.velocity += forceVector;

        #endregion

        #region Aiming V1
        float axisL_Vertical = Input.GetAxis("P" + player + "L Vertical");

        bool deep1 = Physics2D.OverlapCircle(deepCheck1.position, deepRadius, Platforms);
        bool deep2 = Physics2D.OverlapCircle(deepCheck2.position, deepRadius, Platforms);
        bool side1 = Physics2D.OverlapCircle(sideCheck1.position, sideRaius, Platforms);
        bool side2 = Physics2D.OverlapCircle(sideCheck2.position, sideRaius, Platforms);
        //bool top1 = Physics2D.OverlapCircle(topCheck1.position, sideRaius, Platforms);
        //bool top2 = Physics2D.OverlapCircle(topCheck2.position, sideRaius, Platforms);

        //Flip the Character
        if (axisL_Horizontal > 0 && !facingRight)
        {
            Flip();
        }
        else if (axisL_Horizontal < 0 && facingRight)
        {
            Flip();
        }
        if (axisL_Horizontal > 0 || axisL_Horizontal < 0)
        {
            if (axisL_Vertical == 0)
            {
                #region 0°
                if (!side1)
                {
                    if (!side2)
                    {
                        //0°
                        aimDirectionDegr = 0;
                    }
                    else
                    {
                        aimDirectionDegr = 45;
                    }
                }
                else
                {
                    aimDirectionDegr = 90;
                }
                #endregion
            }
            else if (axisL_Vertical < 0)
            {
                #region 45°
                if (!side1)
                {
                    //45°
                    aimDirectionDegr = 45;
                }
                else
                {
                    aimDirectionDegr = 90;
                }
                #endregion
            }
            else if (axisL_Vertical > 0)
            {
                #region 315°
                if (!deep1)
                {
                    //315°
                    aimDirectionDegr = 315;
                }
                else
                {
                    if (!side1)
                    {
                        if (!side2)
                        {
                            //0°
                            aimDirectionDegr = 0;
                        }
                        else
                        {
                            aimDirectionDegr = 45;
                        }
                    }
                    else
                    {
                        aimDirectionDegr = 90;
                    }
                }
                #endregion
            }
        }
        else if (axisL_Horizontal == 0)
        {
            if (axisL_Vertical < 0)
            {
                #region 90°
                //90°
                aimDirectionDegr = 90;
                #endregion
            }
            else if (axisL_Vertical > 0)
            {
                #region 270°
                if (!deep1)
                {
                    if (!deep2)
                    {
                        //270°
                        aimDirectionDegr = 270;
                    }
                    else
                    {
                        if (!side1)
                        {
                            if (!side2)
                            {
                                //0°
                                aimDirectionDegr = aimDirectionDegr = 315;
                            }
                            else
                            {
                                aimDirectionDegr = 45;
                            }
                        }
                        else
                        {
                            aimDirectionDegr = 90;
                        }
                    }
                }
                else
                {
                    if (!side1)
                    {
                        if (!side2)
                        {
                            //0°
                            aimDirectionDegr = 0;
                        }
                        else
                        {
                            aimDirectionDegr = 45;
                        }
                    }
                    else
                    {
                        aimDirectionDegr = 90;
                    }
                }
                #endregion
            }
        }
        #endregion

        anim.SetFloat("moveHori", axisL_Horizontal);

        if (axisL_Horizontal == 0)
        {
            anim.SetBool("standing", true);
        }
        else
        {
            anim.SetBool("standing", false);
        }
    }

    void Update()
    {
        if (blockedInputs)
            return;

        anim.SetFloat("moveVerti", myRigidbody.velocity.y);

        #region Jumping
        //Player 1 Jump -> Modify so that every player can control their own character
        //Double jumping possible
        if (Input.GetButton("P" + player + " Button A"))
        {
            //walljump
            if (!isGrounded && isFacing && !buttonA_GCD && isJumpReleased)
            {
                #region Old Walljump
                //Flip();
                //blockedInputs = true;
                //StartCoroutine(IEUnlockInputsSelf(0.1f));
                //myRigidbody.velocity = new Vector2(0, 0);
                //myRigidbody.angularVelocity = 0;
                //myRigidbody.AddForce(new Vector2(-myRotationObject.transform.localScale.z * 1000, jumpForce * wallJumpMulti));

                //isJumpReleased = false;

                //isDoubleJumping = false;
                //TripleJumped = false;

                //buttonA_GCD = true;
                //StartCoroutine(ButtonA_GCD());
                #endregion

                int tempFacing;
                if (facingRight)
                {
                    tempFacing = 1;
                }
                else
                {
                    tempFacing = 2;
                }
                if (tempFacing != hasFaced && Physics2D.OverlapCircle(faceCheck.position, faceRadius, Platforms))
                {
                    //Animation
                    anim.SetTrigger("walljump");

                    myRigidbody.velocity = new Vector2(0, 0);
                    myRigidbody.angularVelocity = 0;
                    myRigidbody.AddForce(new Vector2(0, jumpForce));

                    isJumpReleased = false;

                    buttonA_GCD = true;
                    StartCoroutine(ButtonA_GCD());
                    //isDoubleJumping = false;
                    if (facingRight)
                    {
                        hasFaced = 1;
                    }
                    else
                    {
                        hasFaced = 2;
                    }
                }
            }
            //jump - doublejump
            else if (!isDoubleJumping && !buttonA_GCD && isJumpReleased || TripleJumpPassiveActive && !TripleJumped && !buttonA_GCD && isJumpReleased)
            {
                //Animation
                anim.SetTrigger("jump");
                //Debug.Log("Jump!");

                myRigidbody.velocity = new Vector2(0, 0);
                myRigidbody.angularVelocity = 0;
                myRigidbody.AddForce(new Vector2(0, jumpForce));

                isJumpReleased = false;

                buttonA_GCD = true;
                StartCoroutine(ButtonA_GCD());

                if (isDoubleJumping && !TripleJumped && TripleJumpPassiveActive && !isGrounded)
                {
                    TripleJumped = true;
                }
                if (!isDoubleJumping && !isGrounded)
                {
                    isDoubleJumping = true;
                }
            }

        }
        //jumping-button released
        else if (Input.GetButtonUp("P" + player + " Button A"))
        {
            //Animation

            isJumpReleased = true;
        }
        #endregion

        #region Stomping
        if (Input.GetButton("P" + player + " Button B") && !buttonB_GCD)
        {
            if (!isGrounded)
            {
                buttonB_GCD = true;

                anim.SetBool("stomping", true);

                myRigidbody.velocity = new Vector2(0, 0);
                myRigidbody.angularVelocity = 0;
                myRigidbody.AddForce(new Vector2(0, -stompForce));
            }
        }
        else if (Input.GetButtonUp("P" + player + " Button B"))
        {
            anim.SetBool("stomping", false);
            buttonB_GCD = false;
            myRigidbody.velocity = new Vector2(0, myRigidbody.velocity.y / 4);
            myRigidbody.angularVelocity = 0;
        }
        #endregion

        #region Throwing
        if (Input.GetButton("P" + player + " Button X"))
        {
            if (!buttonX_GCD && !isThrowing)
            {
                buttonX_GCD = true;
                StartCoroutine(ButtonX_GCD());
                CheckCollision(faceCheck, faceRadius, "throw");
            }
            else if (isThrowing)
            {
                float aimDirX = 0;
                float aimDirY = 0;

                if (aimDirectionDegr == 0)
                {
                    aimDirX = -10 * myRotationObject.transform.localScale.z;
                    aimDirY = 0;
                }
                else if (aimDirectionDegr == 45)
                {
                    aimDirX = -8 * myRotationObject.transform.localScale.z;
                    aimDirY = 8f;
                }
                else if (aimDirectionDegr == 90)
                {
                    aimDirX = 0;
                    aimDirY = 15;
                }
                else if (aimDirectionDegr == 315)
                {
                    aimDirX = -8 * myRotationObject.transform.localScale.z;
                    aimDirY = -8;
                }
                else if (aimDirectionDegr == 270)
                {
                    aimDirX = 0;
                    aimDirY = -15;
                }

                holdenEnemy.transform.localPosition = new Vector2(aimDirX, aimDirY);
            }
        }
        else if (Input.GetButtonUp("P" + player + " Button X") && isThrowing)
        {
            holdenEnemy.transform.localEulerAngles = new Vector2(0, 0);
            holdenEnemy.transform.localScale = new Vector3(1, 1, 1);

            holdenRigidbody.isKinematic = false;
            holdenEnemy.transform.parent = null;

            holdenRigidbody.velocity = new Vector2(0, 0);
            holdenRigidbody.angularVelocity = 0;

            float forceX = 0;
            float forceY = 0;

            if (aimDirectionDegr == 0)
            {
                forceX = -myRotationObject.transform.localScale.z * jumpForce * 1.5f;
                forceY = 0;
            }
            else if (aimDirectionDegr == 45)
            {
                forceX = -myRotationObject.transform.localScale.z * jumpForce;
                forceY = jumpForce;
            }
            else if (aimDirectionDegr == 90)
            {
                forceX = 0;
                forceY = jumpForce * 1.5f;
            }
            else if (aimDirectionDegr == 315)
            {
                forceX = -myRotationObject.transform.localScale.z * jumpForce;
                forceY = -jumpForce * 2;
            }
            else if (aimDirectionDegr == 270)
            {
                forceX = 0;
                forceY = -jumpForce * 1.5f;
            }

            holdenRigidbody.AddForce(new Vector2(forceX, forceY));

            StartCoroutine(IEUnlockInputsEnemy(1));

            isThrowing = false;
        }
        #endregion

        #region Button LB
        if (Input.GetButton("P" + player + " Button LB") && !buttonLB_GCD)
        {
            //Debug.Log("Button LB pressed!");
            spell_LB.SendMessage("Go", SendMessageOptions.RequireReceiver);
            anim.SetTrigger("cast");
            buttonLB_GCD = true;
        }
        else if (Input.GetButtonUp("P" + player + " Button LB") && buttonLB_GCD)
        {
            //Debug.Log("Button LB released!");
            spell_LB.SendMessage("Stop", SendMessageOptions.RequireReceiver);
            buttonLB_GCD = false;
        }
        #endregion

        #region Button RB
        if (Input.GetButton("P" + player + " Button RB") && !buttonRB_GCD)
        {
            //Debug.Log("Button RB pressed!");
            spell_RB.SendMessage("Go", SendMessageOptions.RequireReceiver);
            anim.SetTrigger("cast");
            buttonRB_GCD = true;
        }
        else if (Input.GetButtonUp("P" + player + " Button RB") && buttonRB_GCD)
        {
            //Debug.Log("Button RB released!");
            spell_RB.SendMessage("Stop", SendMessageOptions.RequireReceiver);
            buttonRB_GCD = false;
        }
        #endregion

        #region Trigger LT
        if (Input.GetAxis("P" + player + " Trigger LT") > 0.5f && !triggerLT_GCD)
        {
            //Debug.Log("Trigger LT pulled!");
            spell_LT.SendMessage("Go", SendMessageOptions.RequireReceiver);
            anim.SetTrigger("cast");
            triggerLT_GCD = true;
        }
        else if (Input.GetAxis("P" + player + " Trigger LT") < 0.5f && triggerLT_GCD)
        {
            //Debug.Log("Trigger LT released!");
            spell_LT.SendMessage("Stop", SendMessageOptions.RequireReceiver);
            triggerLT_GCD = false;
        }
        #endregion

        #region Trigger RT
        if (Input.GetAxis("P" + player + " Trigger RT") > 0.5f && !triggerRT_GCD)
        {
            Debug.Log("Trigger LT pulled!");
            spell_RT.SendMessage("Go", SendMessageOptions.RequireReceiver);
            anim.SetTrigger("cast");
            triggerRT_GCD = true;

            //anim.SetTrigger("spellGo");
        }
        else if (Input.GetAxis("P" + player + " Trigger RT") < 0.5f && triggerRT_GCD)
        {
            Debug.Log("Trigger LT released!");
            spell_RT.SendMessage("Stop", SendMessageOptions.RequireReceiver);
            triggerRT_GCD = false;
        }
        #endregion
    }

    #region Global Cooldown / Enumerators
    IEnumerator ButtonA_GCD()
    {
        yield return button_GCD;
        buttonA_GCD = false;
    }

    IEnumerator ButtonB_GCD()
    {
        yield return button_GCD;
        buttonB_GCD = false;
    }

    IEnumerator ButtonX_GCD()
    {
        yield return new WaitForSeconds(ThrowCoolDown);
        buttonX_GCD = false;
    }

    IEnumerator IEUnlockInputsEnemy(float waitfor)
    {
        yield return new WaitForSeconds(waitfor);
        holdenEnemy.GetComponent<CharController>().blockedInputs = false;
    }
    IEnumerator IEUnlockInputsSelf(float waitfor)
    {
        yield return new WaitForSeconds(waitfor);
        blockedInputs = false;
    }

    #endregion

    void Flip()
    {
        facingRight = !facingRight;

        if (facingRight)
        {
            myRotationObject.transform.eulerAngles = new Vector3(0, 180, 0);
        }
        else if (!facingRight)
        {
            myRotationObject.transform.eulerAngles = new Vector3(0, 0, 0);
        }

        //Debug.Log(myRotationObject.transform.eulerAngles);

        Vector3 theScale = myRotationObject.transform.localScale;
        theScale.z *= -1;
        myRotationObject.transform.localScale = theScale;
    }

    void CheckCollision(Transform check, float radius, string reason)
    {
        //deal dmg to colliding units
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(check.position, radius);

        foreach (Collider2D enemycol in hitEnemies)
        {
            if (enemycol.tag == "Player" && enemycol.transform.root.gameObject != this.gameObject)
            {
                holdenEnemy = enemycol.transform.root.gameObject;
                //Debug.Log(holdenEnemy);

                if (reason == "stomp")
                {
                    myRigidbody.velocity = new Vector2(0, 0);
                    enemycol.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, -jumpForce));
                }
                else if (reason == "throw" && !isThrowing)
                {
                    //Debug.Log("holding!");

                    isThrowing = true;

                    holdenEnemy.GetComponent<CharController>().blockedInputs = true;
                    holdenRigidbody = holdenEnemy.GetComponent<Rigidbody2D>();

                    holdenEnemy.transform.SetParent(transform);

                    holdenRigidbody.isKinematic = true;
                    holdenEnemy.transform.localPosition = new Vector2(5, 0);
                }
            }
        }
    }

    //MAYBE WATER?!
    //void OnTriggerStay2D(Collider2D col)
    //{
    //    if (col.tag == "Water")
    //    {
    //        isDoubleJumping = false;
    //        GetComponent<PlayerHealthController>().AdjustHealth(-1);
    //    }
    //}

    public void StartControls()
    {
        blockedInputs = false;
        myRigidbody.isKinematic = false;

        spell_LB = myRotationObject.transform.Find("Spell_LB").gameObject;
        spell_LT = myRotationObject.transform.Find("Spell_LT").gameObject;
        spell_RT = myRotationObject.transform.Find("Spell_RT").gameObject;
        spell_RB = myRotationObject.transform.Find("Spell_RB").gameObject;
    }

    public void StopControls()
    {
        blockedInputs = true;
        myRigidbody.isKinematic = true;
    }
}

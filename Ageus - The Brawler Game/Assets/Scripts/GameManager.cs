﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
//using UnityEditor.SceneManagement;

public class GameManager : MonoBehaviour
{

    public Dictionary<int, bool> Players = new Dictionary<int, bool>(); //int...id  |  bool...alive
    public List<Sprite> spritelist = new List<Sprite>();

    public Image UIImage;
    public Image WinnerText;
    public GameObject go;

    public void CheckForWin(int killedID)
    {
        Players[killedID] = false;

        int countAlive = 0;
        int lastPlayerAliveID = 0;

        for (int i = 0; i < Players.Count; i++)
        {
            if (Players[i])
            {
                countAlive++;
                lastPlayerAliveID = i;
            }
        }
        if (countAlive == 1)
        {
            Debug.Log("Player " + lastPlayerAliveID + " has won!");
            for (int i = 0; i < Players.Count; i++)
            {
                GameObject.Find("Player" + i).GetComponent<CharController>().enabled = false;
            }

            UIImage.enabled = true;
            WinnerText.sprite = spritelist[lastPlayerAliveID];
            go.SetActive(true);

            StartCoroutine(CustomUpdate());

            //ZEIGE GUI - DER GEWINNER WIRD BEKANNT GEGEBEN UND MAN KANN NEU STARTEN
        }
        else if (countAlive < 1)
        {
            UIImage.enabled = true;
            go.SetActive(true);
            WinnerText.enabled = false;
            Debug.Log("Everyone is DEAD!");
            StartCoroutine(CustomUpdate());
        }
    }

    IEnumerator CustomUpdate()
    {
        while (true)
        {
            if (Input.GetButton("P" + 0 + " Button A"))
            {
                Application.LoadLevel(0);
            } 
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class DeathZone : MonoBehaviour
{
    public int DeathZoneDMG;

    void OnCollisionStay2D(Collision2D col)
    {
        PlayerHealthController phc;
        if ((phc = col.gameObject.GetComponent<PlayerHealthController>()) != null)
        {
            if (!phc.untargetable)
            {
                phc.AdjustHealth(DeathZoneDMG);
            }
            Destroy(col.gameObject);
        }
    }
}
